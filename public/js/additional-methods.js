function performClick(elemId) {
  var elem = document.getElementById(elemId);
  if (elem && document.createEvent) {
    var evt = document.createEvent("MouseEvents");
    evt.initEvent("click", true, false);
    elem.dispatchEvent(evt);
  }
}

var imagenes = [0, 1, 2];

function showImage(src, target) {
  var fr = new FileReader();
  // when image is loaded, set the src of the image where you want to display it
  fr.onload = function (e) { target.setAttribute("src", this.result); };
  fr.readAsDataURL(src.files[0]);
}

function addImg() {

  if (imagenes.length == 0) {
    window.alert("El maximo de imagenes son 3");
    return null;
  }
  var fr = new FileReader();
  var input = document.getElementById("inputImg");
  var zonaImagenes = document.getElementById("imgs");
  console.log(imagenes.length);

  var imgElement = document.createElement("img");
  showImage(input, imgElement);
  imgElement.classList.add("img-fluid");

  var rmvButton = document.createElement("button");
  rmvButton.type = "button";
  rmvButton.classList.add("btn");
  rmvButton.classList.add("btn-outline-danger");
  rmvButton.innerHTML = "X";
  var numImg = imagenes.shift();

  var hdnInput = input.cloneNode(true);
  hdnInput.id = "img" + numImg;
  hdnInput.name = "img" + numImg;
  hdnInput.setAttribute("style", "display: none;");


  var container = document.createElement("div");
  container.appendChild(rmvButton);
  container.appendChild(document.createElement("br"));
  container.appendChild(imgElement);
  container.appendChild(hdnInput);
  container.setAttribute("style", "margin-bottom: 10px");
  zonaImagenes.appendChild(container);

  rmvButton.onclick = function () {
    zonaImagenes.removeChild(container);
    imagenes.push(numImg);
  }
}
