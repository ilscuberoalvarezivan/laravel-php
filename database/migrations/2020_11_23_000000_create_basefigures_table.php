<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseFiguresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_figures', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('img');
            $table->string('colors');
            $table->date('created_at');
            $table->double('price', 8, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basefigures');
        Schema::dropIfExists('base_figures');
    }
}