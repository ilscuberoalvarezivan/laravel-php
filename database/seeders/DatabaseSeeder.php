<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FigureSeeder::class);
    }
}

class FigureSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table("figures")->insert([
            "name"=>"GAT-X105 Strike Gundam",
            "img"=>"https://vignette.wikia.nocookie.net/gundam/images/5/5c/Gat-x105.jpg",
            "description"=>"The GAT-X105 Strike Gundam is a prototype multi-mode mobile suit featured in the anime Mobile Suit Gundam SEED.",
            "painted"=>true,
            "modified"=>false,
            "damaged"=>false,
            "diorama"=>false,
            "scale"=>"1/144",
            "price"=>80.50,
            "img_preview"=>"https://vignette.wikia.nocookie.net/gundam/images/5/5c/Gat-x105.jpg",
            "created_at"=>"2020-11-11",
            "updated_at"=>"2020-11-11",
        ]);
        
        DB::table("figures")->insert([
            "name"=>"ASW-G-08 Gundam Barbatos Lupus Rex",
            "img"=>"https://static.wikia.nocookie.net/gundam/images/5/5a/ASW-G-08_Gundam_Barbatos_Lupus_Rex_%28Front%29.png",
            "description"=>"The ASW-G-08 Gundam Barbatos Lupus Rex is the main mobile suit featured in the second half of Mobile Suit Gundam IRON-BLOODED ORPHANS second season.",
            "painted"=>false,
            "modified"=>true,
            "damaged"=>false,
            "diorama"=>false,
            "scale"=>"1/100",
            "price"=>100.50,
            "img_preview"=>"https://static.wikia.nocookie.net/gundam/images/5/5a/ASW-G-08_Gundam_Barbatos_Lupus_Rex_%28Front%29.png",
            "created_at"=>"2020-11-11",
            "updated_at"=>"2020-11-11",
        ]);
        
        DB::table("figures")->insert([
            "name"=>"GN-0000GNHW/7SG 00 Gundam Seven Sword/G",
            "img"=>"https://static.wikia.nocookie.net/gundam/images/4/41/00_7s_Gundam_Front.jpg",
            "description"=>"The GN-0000GNHW/7SG 00 Gundam Seven Sword/G, is a variant of the GN-0000/7S 00 Gundam Seven Sword.",
            "painted"=>false,
            "modified"=>false,
            "damaged"=>true,
            "diorama"=>false,
            "scale"=>"1/60",
            "price"=>200.50,
            "img_preview"=>"https://static.wikia.nocookie.net/gundam/images/4/41/00_7s_Gundam_Front.jpg",
            "created_at"=>"2020-11-11",
            "updated_at"=>"2020-11-11",
        ]);

        DB::table("figures")->insert([
            "name"=>"RX-0 Unicorn Gundam",
            "img"=>"https://static.wikia.nocookie.net/gundam/images/f/f0/Rx0uc-ova.jpg",
            "description"=>"The RX-0 Unicorn Gundam is the titular prototype mobile suit of the Mobile Suit Gundam Unicorn novel, its OVA adaptation and the television re-cut.",
            "painted"=>false,
            "modified"=>false,
            "damaged"=>false,
            "diorama"=>true,
            "scale"=>"1/60",
            "price"=>200.50,
            "img_preview"=>"https://static.wikia.nocookie.net/gundam/images/f/f0/Rx0uc-ova.jpg",
            "created_at"=>"2020-11-11",
            "updated_at"=>"2020-11-11",
        ]);

        DB::table("base_figures")->insert([
            "name"=>"1/100 Graze",
            "img"=>"https://www.gundam.my/blog/wp-content/uploads/2016/06/graze_100_2_2048x2048.jpg",
            "colors"=>"09ab34,#076e22,#627065,#ffee00",
            "price"=>80.50,
            "created_at"=>"2020-11-11",

        ]);
        DB::table("base_figures")->insert([
            "name"=>"1/100 Gundam RX-78-2",
            "img"=>"https://images-na.ssl-images-amazon.com/images/I/71XJlnf8v2L._AC_SL1500_.jpg",
            "colors"=>"#ffffff,#ffea00,#0026ff,#ff0000,#000000",
            "price"=>90.99,
            "created_at"=>"2020-11-11",

        ]);
        DB::table("base_figures")->insert([
            "name"=>"1/144 Gundam Astaroth Origin",
            "img"=>"https://images-na.ssl-images-amazon.com/images/I/71efW0%2Bu3NL._AC_SY679_.jpg",
            "colors"=>"#d60000,#780000,#ffffff,#b5b5b5,#000000",
            "price"=>64.50,
            "created_at"=>"2020-11-11",

        ]);
    }
}
