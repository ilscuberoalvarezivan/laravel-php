<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Figure;


class FiguresSingleController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show(Request $request, $id)
    {
        $figure = Figure::where("id", $id)->first();
        if($figure==null){
            abort(404);
        }

        return view("figure-single", ["figure" => $figure, "imgs" => explode(":",$figure->img)]);
    }
}
