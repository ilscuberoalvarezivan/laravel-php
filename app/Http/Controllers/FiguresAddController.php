<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Figure;
use App\Services\UploadFileService;

use App\Exceptions\UploadFileException;
use App\Exceptions\FileFormatException;

class FiguresAddController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show(Request $request)
    {
        return view("figure-modify");
    }

    public function new(Request $request, UploadFileService $UploadFileService)
    {
        $id="";
        try{
            $imgDirs="";
            $destinationPath = 'storage/products';
            $imgPreview=null;
            for($i=0;$i<3;$i++) {
                if ($request->hasFile('img'.$i)) {
                    $img = $request->file('img'.$i);
                    $this->uploadService = $UploadFileService;
                    $imgPath=$this->uploadService->uploadFile($request->file('imagen'));
                    $imgDirs=$imgDirs . $imgPath . ":";
                    if($imgPreview==null){
                        $imgPreview=$imgPath;
                    }
                    
                }
            }
            if ($imgPreview == null){
                throw new UploadFileException('File not defined');
            }
    
            $imgDirs=substr($imgDirs, 0, -1);

            $figure=new Figure();
            $figure->name=$request->input("name");
            $figure->description=$request->input("desc");
            $figure->painted=$request->has("painted");
            $figure->modified=$request->has("modified");
            $figure->damaged=$request->has("damaged");
            $figure->diorama=$request->has("diorama");
            $figure->scale=$request->input("scale");
            $figure->price=$request->input("price");
            $figure->img=$imgDirs;
            $figure->img_preview=$imgPreview;
            $figure->save();
            $id=$figure->id;
        } catch (UploadFileException | FileFormatException $exception) {
            $this->error = $exception->customMessage();
        } catch ( \Illuminate\Database\QueryException $exception) {
            $this->error = "Error con los datos introducidos";
        }

        if($this->error!=null){
            return redirect()->action([FiguresAddController::class, 'show'], ['error' => $id])->withError($this->error);
        }
        

        return redirect()->action([FiguresSingleController::class, 'show'], ['id' => $id]);
    }
}