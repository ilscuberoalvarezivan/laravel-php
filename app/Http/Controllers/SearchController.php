<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Figure;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        return $request->texto;
        $nombre = Figure::where("name","like",$request->texto."%")->get();
        return view("figures",["figures" => $nombre]);
    }
}
