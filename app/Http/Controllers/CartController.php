<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CartListRequest;
use App\Models\Figure;


class CartController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show(Request $request)
    {
        $carrito = $request->session()->get('carrito', []);
        return view("cart",["carrito" => $carrito]);
    }

    public function addToChart(CartListRequest $request, $id)
    {
        $figure = Figure::where("id", $id)->first();

        $carrito = $request->session()->get('carrito', []);
        array_push($carrito, ["id"=>$figure->id,"img_preview"=>$figure->img_preview,"name"=>$figure->name,"price"=>$figure->price]);
        $request->session()->put('carrito', $carrito);
        
        return redirect()->action([CartController::class, 'show']);
    }
    public function removeToChart(CartListRequest $request, $pos)
    {
        $carrito = $request->session()->get('carrito', []);
        array_splice($carrito,$pos,1);
        $request->session()->put('carrito',$carrito);
        //Hacer la función para borrar
        return redirect(url()->previous());
    }
}