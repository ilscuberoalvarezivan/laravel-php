<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Figure;


class FiguresController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show(Request $request)
    {

        $search=$request->input("search");

        $painted=$request->input("painted");
        $modified=$request->input("modified");
        $damaged=$request->input("damaged");
        $diorama=$request->input("diorama");

        $s_160=$request->input("1/60");
        $s_1100=$request->input("1/100");
        $s_1144=$request->input("1/144");
        $s_sd=$request->input("sd");
        $s_other=$request->input("other");

        $figura=new Figure();
        if($painted=="true") $figura=$figura->painted();
        if($modified=="true") $figura=$figura->modified();
        if($damaged=="true") $figura=$figura->damaged();
        if($diorama=="true") $figura=$figura->diorama();
        if($search!="") $figura=$figura->boxSearch($search);

        if($s_160=="true") $figura=$figura->scaleSearch("1/60");
        if($s_1100=="true") $figura=$figura->scaleSearch("1/100");
        if($s_1144=="true") $figura=$figura->scaleSearch("1/144");
        if($s_sd=="true") $figura=$figura->scaleSearch("SD");
        if($s_other=="true") $figura=$figura->scaleSearch("Other");

        $figures=$figura->get();

        if($request->ajax()){
            return view("filtered",["figures" => $figures]);
        }

        return view("figures",["figures" => $figures,"search"=>$search]);
    }
}