<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BaseFigure;


class FigureCustomController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show(Request $request)
    {

        $base=$request->input("base");
        
        if($base==null){
            $base=1;
        }
        $figures=BaseFigure::all();
        $select=BaseFigure::where("id",$base)->first();
        $colors=explode(",",$select->colors);
        $img=$select->img;
        $price=$select->price;

        if($request->ajax()){
            return ["base" => $base,"figures" => $figures, "img" => $img, "colors" => $colors, "price" => $price];
        }
        return view("figure-custom",["base" => $base,"figures" => $figures, "img" => $img, "colors" => $colors, "price" => $price]);
    }
}