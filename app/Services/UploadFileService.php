<?php

namespace App\Services;

use App\Exceptions\UploadFileException;
use App\Exceptions\FileFormatException;

class UploadFileService
{

    function uploadFile($file)
    {

        
        if($file == null || strpos($file->getClientOriginalName(),".")===false){
            throw new FileFormatException("File is not a Picture");
        }

        $originalFile = $file->getClientOriginalName();
        $fileFormat=strtoupper(explode(".",$originalFile)[1]);
        
        if($fileFormat!="PNG"&&$fileFormat!="JPG"&&$fileFormat!="JPGE"){
            throw new FileFormatException("File is not a Picture");
        }

        $destinationPath = 'storage/products';
        $file->move($destinationPath, $originalFile);
        return $destinationPath . "/" . $originalFile;
    }
}
