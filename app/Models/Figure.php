<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Figure extends Model
{
    public function scopePainted(){
        
        return $this->where('painted',true);
    }

    public function scopeModified(){
        return $this->where('modified',true);
    }

    public function scopeDamaged(){
        return $this->where('damaged',true);
    }

    public function scopeDiorama(){
        return $this->where('diorama',true);
    }

    public function scaleSearch($search){
        return $this->where("scale","LIKE","%".$search."%");
    }

    public function boxSearch($search){
        return $this->where("name","LIKE","%".$search."%");
    }
}
