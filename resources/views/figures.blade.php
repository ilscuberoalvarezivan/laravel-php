
@extends('layouts.layout')

@section('content')

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="/index">Inicio</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Modelos</strong></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">

        <div class="row mb-5">
          <div class="col-md-9 order-2">

            <div class="row">
              <div class="col-md-12 mb-5">
                <div class="float-md-left mb-4"><h2 class="text-black h5">Modelos</h2></div>
                <div class="d-flex">
                  <div class="btn-group mr-1 ml-md-auto">
                    <button type="button" class="btn btn-secondary btn-sm dropdown-toggle" id="dropdownMenuReference" data-toggle="dropdown">Ordenar por</button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuReference">
                      <a class="dropdown-item" href="#">Fecha, mas reciente</a>
                      <a class="dropdown-item" href="#">Fecha, mas antiguo</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Precio, menor a mayor</a>
                      <a class="dropdown-item" href="#">Precio, mayor a menor</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mb-5" id="figures">


              @foreach ($figures as $figure)
              <div class="col-sm-6 col-lg-4 mb-4" data-aos="fade-up">
                <div class="block-4 text-center border">
                  <figure class="block-4-image">
                    <a href="/figures/{{$figure->id}}"><img src="{{asset($figure->img_preview)}}" alt="Image placeholder" class="img-fluid"></a>
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="/figures/{{$figure->id}}">{{$figure->name}}</a></h3>
                    <p class="text-primary">{{$figure->description}}</p>
                    <p class="text-primary font-weight-bold">{{$figure->price}}€</p>
                  </div>
                </div>
              </div>
            @endforeach


            </div>
            <div class="row" data-aos="fade-up">
              <div class="col-md-12 text-center">
                <div class="site-block-27">
                  <ul>
                    <li><a href="#">&lt;</a></li>
                    <li class="active"><span>1</span></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&gt;</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3 order-1 mb-5 mb-md-0">



            <div class="border p-4 rounded mb-4">
              <form id="fcheck" action="figures" method="get">
              <div   style="margin-bottom: 20px;" class="site-block-top-search">
                <span class="icon icon-search2"></span>
                <input name="search" type="text" class="form-control border-0" placeholder="Buscar" value="{{$search ?? ''}}">
              </div>

              <div class="mb-4">
                <h3 class="mb-3 h6 text-uppercase text-black d-block">Personalizacion</h3>
                <label for="s_sm" class="d-flex">
                    <input onclick="asinc_filter()" type="checkbox" id="s_sm"  name="painted" class="mr-2 mt-1"> <span class="text-black">Pintado</span>
                </label>
                <label for="s_sm" class="d-flex">
                <input type="hidden" name="modified" value="0" />
                  <input onclick="asinc_filter()" type="checkbox" id="s_sm" name="modified" class="mr-2 mt-1"> <span class="text-black">Modificado</span>
                </label>
                <label for="s_sm" class="d-flex">
                <input type="hidden" name="damaged" value="0" />
                  <input onclick="asinc_filter()" type="checkbox" id="s_sm" name="damaged" class="mr-2 mt-1"> <span class="text-black">Dañado</span>
                </label>
                <label for="s_sm" class="d-flex">
                <input type="hidden" name="diorama" value="0" />
                  <input onclick="asinc_filter()" type="checkbox" id="s_sm" name="diorama" class="mr-2 mt-1"> <span class="text-black">Diorama</span>
                </label>
              </div>

              <div class="mb-4">
                <h3 class="mb-3 h6 text-uppercase text-black d-block">Escala</h3>
                <label for="s_sm" class="d-flex">
                  <input onclick="asinc_filter()" type="checkbox" id="s_sm" name="1/60" class="mr-2 mt-1"> <span class="text-black">1/60</span>
                </label>
                <label for="s_sm" class="d-flex">
                  <input onclick="asinc_filter()" type="checkbox" id="s_sm"  name="1/100" class="mr-2 mt-1"> <span class="text-black">1/100</span>
                </label>
                <label for="s_sm" class="d-flex">
                  <input onclick="asinc_filter()" type="checkbox" id="s_sm"  name="1/144" class="mr-2 mt-1"> <span class="text-black">1/144</span>
                </label>
                <label for="s_sm" class="d-flex">
                  <input onclick="asinc_filter()" type="checkbox" id="s_sm"  name="sd" class="mr-2 mt-1"> <span class="text-black">SD</span>
                </label>
                <label for="s_sm" class="d-flex">
                  <input onclick="asinc_filter()" type="checkbox" id="s_sm"  name="other" class="mr-2 mt-1"> <span class="text-black">Otros</span>
                </label>
              </div>
              </form>
            </div>
            <button type="button" class="btn btn-secondary btn-sm">Pedido Personalizado</button>
          </div>
        </div>

      </div>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
  <script>
    function asinc_filter(){
      $.ajax({
               url:'/figures',
               data:{
                "painted":$('[name="painted"]').is(":checked"),
                "modified":$('[name="modified"]').is(":checked"),
                "damaged":$('[name="damaged"]').is(":checked"),
                "diorama":$('[name="diorama"]').is(":checked"),
                "1/60":$('[name="1/60"]').is(":checked"),
                "1/100":$('[name="1/100"]').is(":checked"),
                "1/144":$('[name="1/144"]').is(":checked"),
                "sd":$('[name="sd"]').is(":checked"),
                "other":$('[name="other"]').is(":checked"),
                "search":$('[name="search"]').val()
                 },
               type:'get',
               success:  function (response) {
                 console.log(response);
                  $("#figures").html(response);

               }
            });
    }
  </script>
  @endsection
