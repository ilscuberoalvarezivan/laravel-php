@extends('layouts.layout')

@section('content')

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="/index">Inicio</a> <span class="mx-2 mb-0">/</span><a
              href="/figures">Modelos</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Añadir
              Producto</strong></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
      <form id="form" method="post" action="/figures/add" enctype="multipart/form-data" >
      {{ csrf_field() }}
        <div class="row">
          <div class="col-md-5">
            <div class="form-group">
              <label for="name">
                <h4 class="text-black">Nombre</h4>
              </label>
              <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
              <label for="desc">
                <h4 class="text-black">Descripcion</h4>
              </label>
              <textarea class="form-control" id="desc" name="desc" rows="3"></textarea>
            </div>
            <div class="form-group">
              <label for="name">
                <h4 class="text-black">Precio Base</h4>
              </label>
              <input type="number" class="form-control" id="price" name="price" aria-describedby="priceHelp">
            </div>
          </div>
          <div class="col-md-2">
            <h4 class="text-black">Personalizacion:</h4>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="painted" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                Pintado
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="modified" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                Modificado
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="damaged" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                Dañado
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="diorama" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                Diorama
              </label>
            </div><br>
            <h4 class="text-black">Escala:</h4>
            <div class="form-check">
              <input class="form-check-input" type="radio" value="1/60" name="scale" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                1/60
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" value="1/100" name="scale" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                1/100
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" value="1/144" name="scale" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                1/144
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" value="SD" name="scale" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                SD
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" value="Other" name="scale" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                Otros
              </label>
            </div>

          </div>
          <div class="col-md-5">
            <h4 class="text-black">Imagenes:</h4>
            <div id="imgs" class="border" style="overflow-y: scroll;height: 300px;">
            </div>
            <div class="input-group" style="margin-top: 5px;">
              <input type="file" class="custom-file-input" id="inputImg" onchange="addImg()" style="display: none;">
              <button type="button" class="btn btn-secondary"
                onclick="document.getElementById('inputImg').click()">Añadir Imagen</button>
            </div>
          </div>
        </form>
        </div>
        @if(app('request')->has('error')&&session('error'))
        <div class="row justify-content-md-center" style="margin-top: 25px;margin-bottom: 25px;">
          <div class="alert alert-danger">
              {{ session('error') }}
          </div>
        </div>
        @endif
        <div class="row justify-content-md-center" style="margin-top: 25px;margin-bottom: 25px;">
          <div class="col-md-auto">
            <button type="button" class="btn btn-secondary" onclick="$('#form').submit()">Guardar</button>
          </div>
        </div>
    </div>
    <script src="{{ asset('js/additional-methods.js') }}"></script>
  @endsection
