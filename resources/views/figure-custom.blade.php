
@extends('layouts.layout')

@section('content')

      <div class="bg-light py-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12 mb-0"><a href="#">Inicio</a> <span class="mx-2 mb-0">/</span><a
                href="#">Modelos</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Pedido
                Personalizado</strong></div>
          </div>
        </div>
      </div>

      <div class="site-section">
        <div class="container">
          <div class="row">
            <div class="col-md-5">
            <form action="/figures/custom" id="form" method="get">
              <div class="form-inline">
                <label class="my-1 mr-2" for="select">
                  <h4 class="text-black" style="margin-top: 10px;">Modelo Base:</h4>
                </label>
                <select name="base" class="custom-select my-1 mr-sm-2" id="select">
                @foreach ($figures as $figure)
                  <option 
                  @if ($base == "".$figure->id)
                  selected 
                  @endif
                  value="{{$figure->id}}">{{$figure->name}}</option>
                @endforeach
                </select>
              </div>
              </form>
              <div class="text-center" id="img">
                <img src="{{$img}}" 
                  class="img-fluid"><br></div>
            </div>
            <div class="col-md-4">
              <h4 class="text-black" style="margin-top: 14px;">Paleta de colores:</h4>
              <div class="h-50" id="colors">
                @for ($i = 0; $i < count($colors); $i++)

                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" name="selCol[{{$i}}]" data-toggle="collapse" href="#color{{$i}}"
                    id="defaultCheck{{$i}}">
                  <label class="form-check-label" for="defaultCheck{{$i}}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-square-fill" style="color: {{$colors[$i]}};"
                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2z" />
                    </svg>
                  </label>
                </div>
                <input type="color" class="collapse" id="color{{$i}}" name="color[{{$i}}]">
                <br>
                @endfor
              </div>
              <h4 class="text-black">Detalles:</h4>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                  Panel lining(+1%)
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                  Weathering(+5%)
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" data-toggle="collapse" href="#detalles" type="checkbox" value=""
                  id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                  Detalles adicionales(+20%)
                </label>
              </div>

            </div>
            <div class="col-md-3">
              <div class="border" style="padding: 30px;">
                <p>Precio Base: <strong class="text-primary h4" id="price">{{$price}}€</strong></p>
                <p><a href="#" class="buy-now btn btn-sm btn-primary">Añadir al carro</a></p>
                <p><a href="#" class="buy-now btn btn-sm btn-primary">Comprar ya</a></p>
              </div>
            </div>
          </div>
          <div class="row collapse" id="detalles">
            <div class="col-md-9">
              <br>
              <div class="form-group">
                <label for="exampleFormControlTextarea">
                  <h6 class="text-black">Detalles Adicionales:</h6>
                </label>
                <textarea class="form-control" id="exampleFormControlTextarea" rows="3"></textarea>
              </div>
              <form>
                <div class="form-group">
                  <label for="exampleFormControlFile1">
                    <h6 class="text-black">Imagenes de Referencia:</h6>
                  </label>
                  <input type="file" class="form-control-file" id="exampleFormControlFile1" multiple>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <footer class="site-footer border-top">
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <p>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;
              <script data-cfasync="false"
                src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
              <script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made
              with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank"
                class="text-primary">Colorlib</a>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>

        </div>
    </div>
    </footer>
    </div>
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script type = "text/javascript">
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         $('#select').change(function(){
            //we will send data and recive data fom our AjaxController
            //alert("im just clicked click me");
            $.ajax({
               url:'/figures/custom',
               data:{'base':$('#select').val()},
               type:'get',
               success:  function (response) {
                  console.log(response);
                  $('#img').html('<img src="'+response["img"]+'"class="img-fluid"><br>');
                  $('#price').html(response["price"]+'€');
                  $('#colors').html("");
                  var colors=response["colors"];
                  for (let i = 0; i < colors.length; i++){
                    $('#colors').html($('#colors').html()+
                    '<div class="form-check form-check-inline">'+
                      '<input class="form-check-input" type="checkbox" name="selCol['+i+']" data-toggle="collapse" href="#color'+i+'"'+
                      ' id="defaultCheck'+i+'">' +
                      '<label class="form-check-label" for="defaultCheck'+i+'">' +
                        '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-square-fill" style="color: '+colors[i]+'" '+
                        ' fill="currentColor" xmlns="http://www.w3.org/2000/svg">'+
                          '<path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2z" />'+
                        '</svg>'+
                      '</label>'+
                    '</div>'+
                    '<input type="color" class="collapse" id="color'+i+'" name="color['+i+']">'+
                    '<br>');
                  }

               },
               statusCode: {
                  404: function() {
                     alert('web not found');
                  }
               },
               error:function(x,xs,xt){
                  window.open(JSON.stringify(x));
                  //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
               }
            });
             });
       </script>
  @endsection