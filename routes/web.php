<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Ruta del buscador
Auth::routes();

Route::get('/figures', [App\Http\Controllers\FiguresController::class, 'show']);

Auth::routes();

Route::get('/figures/custom', [App\Http\Controllers\FigureCustomController::class, 'show']);

Auth::routes();

Route::get('/figures/add', [App\Http\Controllers\FiguresAddController::class, 'show']);

Route::post('/figures/add', [App\Http\Controllers\FiguresAddController::class, 'new']);

Auth::routes();

Route::get('/figures/{id}', [App\Http\Controllers\FiguresSingleController::class, 'show']);

Auth::routes();

Route::get('/cart', [App\Http\Controllers\CartController::class, 'show']);

Auth::routes();

Route::get('/cart/add/{id}', [App\Http\Controllers\CartController::class, 'addToChart']);

Auth::routes();

Route::get('/cart/remove/{pos}', [App\Http\Controllers\CartController::class, 'removeToChart']);

Auth::routes();

Route::get('/index', function () {
    return view('index');
});

Auth::routes();

Route::get('/about', function () {
    return view('about');
});

Auth::routes();

/*Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
*/
